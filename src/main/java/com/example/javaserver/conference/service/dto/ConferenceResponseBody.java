package com.example.javaserver.conference.service.dto;

public class ConferenceResponseBody {
    private String roomId;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }
}
