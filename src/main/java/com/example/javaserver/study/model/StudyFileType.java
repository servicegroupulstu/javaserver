package com.example.javaserver.study.model;

public enum StudyFileType {
    WORK,
    TASK,
    QUESTION
}
