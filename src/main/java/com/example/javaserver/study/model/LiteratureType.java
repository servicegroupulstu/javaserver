package com.example.javaserver.study.model;

public enum LiteratureType {
    BOOK,
    WORKBOOK
}