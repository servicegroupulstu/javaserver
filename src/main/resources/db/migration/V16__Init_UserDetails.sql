insert into user_contacts(type, value, user_id)
values ('EMAIL', 'id-anton@gmail.com', 2);

insert into user_contacts(type, value, user_id)
values ('VKONTAKTE', 'vk.com/antonus-patronus', 2);

insert into user_contacts(type, value, user_id)
values ('VKONTAKTE', 'vk.com/pashtet', 3);

insert into user_contacts(type, value, user_id)
values ('TELEGRAM', 't.me/pavel-the-first', 3);

insert into user_contacts(type, value, user_id)
values ('TELEGRAM', 't.me/alewa', 4);

insert into user_contacts(type, value, user_id)
values ('TELEGRAM', 't.me/leontiy', 4);

insert into user_contacts(type, value, user_id)
values ('VKONTAKTE', 'vk.com/mistress-button-architect', 5);

insert into user_contacts(type, value, user_id)
values ('TELEGRAM', 't.me/foreman', 6);

insert into user_contacts(type, value, user_id)
values ('EMAIL', 'sale@ritg.ru', 11);