package com.example.javaserver.repository;

import com.example.javaserver.journal.model.Journal;
import com.example.javaserver.journal.repo.JournalRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

/**
 * @author Anton Mamakin
 **/
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
@RunWith(SpringRunner.class)
public class JournalRepoTest {

    @Autowired
    JournalRepo journalRepo;

    @Test
    @Rollback()
    public void save_and_get_test() {
        Journal journal = new Journal();
        Journal save = journalRepo.save(journal);

        assertNotNull(save.getId());
        Long id = save.getId();
        Optional<Journal> byId = journalRepo.findById(id);
        assertThat(Boolean.TRUE, is(byId.isPresent()));
        journalRepo.delete(byId.get());
        Optional<Journal> byIdDeleted = journalRepo.findById(id);
        assertThat(Boolean.FALSE, is(byIdDeleted.isPresent()));
    }

    @Test
    @Rollback()
    public void bad_get_test() {
        Long id = 1_000_000_000L;
        Optional<Journal> byId = journalRepo.findById(id);
        assertThat(Boolean.FALSE, is(byId.isPresent()));
    }

    @Test(expected = EmptyResultDataAccessException.class)
    @Rollback()
    public void bad_delete_test() {
        Long id = 1_000_000_000L;
        journalRepo.deleteById(id);
    }

    @TestConfiguration
    @EnableJpaRepositories(basePackages = "com.example.javaserver.journal.repo")
    @EntityScan(basePackages = {"com.example.javaserver"})
    static class TestMapperConfiguration {


    }
}

